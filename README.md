About
-----

Ansible role to install and configure hashicorp vault  on a linux system.
reference: https://medium.com/@mitesh_shamra/setup-hashicorp-vault-using-ansible-fa8073a70a56

Variables
---------
tftp_packages:

tftp_root_path:

tftp_server_args:

tftp_setype:

tftp_anon_write:

tftp_home_dir:

tftp_user:

tftp_group:

tftp_mode:

tftp_service:

tftp_server:

tftp_config:


Usage Example
-------------

```yml
tftp_packages:
 - xinetd
 - tftp-server

tftp_root_path: /tftpboot

tftp_server_args: "--secure"

tftp_setype: tftpdir_rw_t

tftp_anon_write: false

tftp_home_dir: false

tftp_user: "root"

tftp_group: "root"

tftp_mode: "0755"

tftp_service: tftp.socket

tftp_server: /usr/sbin/in.tftpd

tftp_config: /usr/lib/systemd/system/tftp.service

roles:
  - { role: ansible_role_tftp, tags: pxe }
```


Testing
-------

```bash
molecule test
```
